/**
 *  @filename   :   epd7in5-demo.ino
 *  @brief      :   7.5inch e-paper display demo
 *  @author     :   Yehui from Waveshare
 *
 *  Copyright (C) Waveshare     August 25 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <SPI.h>
#include "epd7in5b_HD.h"
#include "imagedata.h"
#include "black_bot.h"
#include "red_top.h"
#include "red_bot.h"
#include "offer.h"
#include "black2_bot.h"
#include "black2_top.h"
#include "red2_top.h"
//#include "red2_bot.h"

void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
    Epd epd;
    if (epd.Init() != 0) {
        Serial.print("e-Paper init failed");
        return;
    }
    //epd.DisplaypartBlackOne(gImage_black2_top, gImage_black2_bot, gImage_red2_top, gImage_red_top);

    
    //epd.Displaypart(gImage_offer,350, 266,162,162, 0);
    
    //epd.Displaypart(IMAGE_DATA,350, 266,240,103, 1);
    
    //epd.Displaypart(gImage_red_bot,0, 164,880,264, 1);
    //epd.Displaypart(gImage_red_bot,0, 264,880,264, 1);

    epd.Sleep();
    
}

void loop() {
  // put your main code here, to run repeatedly:

}
